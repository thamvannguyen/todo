package com.gitlab.thamvannguyen.work.manager.impl;


import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.thamvannguyen.core.util.json.TestDataLoader;
import com.gitlab.thamvannguyen.work.dao.entity.Work;
import com.gitlab.thamvannguyen.work.dao.repository.WorkRepository;
import com.gitlab.thamvannguyen.work.dto.Task;
import com.gitlab.thamvannguyen.work.enums.Direction;
import com.gitlab.thamvannguyen.work.enums.OrderBy;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.verify;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@RunWith(DataProviderRunner.class)
public class WorkServiceImplTest {

    private String workPageFile = "WorkPage.json";
    private String workFileName = "Work.json";
    private String taskFileName = "Task.json";

    @Mock
    private WorkRepository workRepository;

    @InjectMocks
    private WorkServiceImpl workService;

    private TestDataLoader testDataLoader = TestDataLoader.builder().path("/data/").build();

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = DateTimeFormatException.class)
    public void shouldThrowDateTimeFormatExceptionWhenCreateWorkWithWrongDateTimeFormat() throws StatusNotFoundException, DateTimeFormatException {
        Task model = prepareTask();
        model.setStartingDate("Wrong datetime format");

        workService.createWork(model);
    }


    @Test(expected = StatusNotFoundException.class)
    public void shouldThrowStatusNotFoundExceptionWhenInvalidStatus() throws StatusNotFoundException, DateTimeFormatException {
        Task model = prepareTask();
        model.setStatus("Invalid status");

        workService.createWork(model);
    }


    @Test
    public void shouldReturnDetailOfWorkWhenCreateWorkSuccess() throws DateTimeFormatException, StatusNotFoundException {
        Task model = prepareTask();
        Work expectedResult = prepareWork();
        ArgumentCaptor<Work> workArgumentCaptor= ArgumentCaptor.forClass(Work.class);

        workService.createWork(model);

        verify(workRepository, Mockito.times(1)).save(workArgumentCaptor.capture());
        Work actualResult = workArgumentCaptor.getValue();
        Assertions.assertThat(actualResult.getId()).isNotBlank();
        Assertions.assertThat(actualResult.getWorkName()).isNotBlank();
        Assertions.assertThat(actualResult.getStatus()).isNotNull();
        Assertions.assertThat(actualResult.getStartingDate()).isNotNull();
        Assertions.assertThat(actualResult.getEndingDate()).isNotNull();
        Assertions.assertThat(actualResult.getWorkName()).isEqualTo(expectedResult.getWorkName());
        Assertions.assertThat(actualResult.getStatus()).isEqualByComparingTo(expectedResult.getStatus());
        Assertions.assertThat(actualResult.getStartingDate()).isEqualTo(expectedResult.getStartingDate());
        Assertions.assertThat(actualResult.getEndingDate()).isEqualTo(expectedResult.getEndingDate());
    }


    @Test(expected = WorkNotFoundException.class)
    public void shouldThrowWorkNotFoundExceptionWhenGetWorkByInvalidId() throws WorkNotFoundException {
        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());

        workService.getWork(Mockito.anyString());
    }


    @Test
    public void shouldReturnDetailOfWorkWhenGetWorkSuccess() throws WorkNotFoundException {
        Work expectedResult = prepareWork();

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.of(prepareWork()));

        Work actualResult = workService.getWork(Mockito.anyString());

        Assertions.assertThat(actualResult).isEqualToComparingFieldByField(expectedResult);
    }


    @Test(expected = DateTimeFormatException.class)
    public void shouldThrowDateTimeFormatExceptionWhenUpdateWorkWithWrongDateTimeFormat() throws StatusNotFoundException, DateTimeFormatException, WorkNotFoundException {
        Task model = prepareTask();
        model.setStartingDate("Wrong datetime format");

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.of(prepareWork()));

        workService.updateWork(model);
    }


    @Test(expected = StatusNotFoundException.class)
    public void shouldThrowStatusNotFoundExceptionExceptionWhenUpdateWorkByInvalidStatus() throws StatusNotFoundException, DateTimeFormatException, WorkNotFoundException {
        Task model = prepareTask();
        model.setStatus("Wrong status");

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.of(prepareWork()));

        workService.updateWork(model);
    }


    @Test(expected = WorkNotFoundException.class)
    public void shouldThrowWorkNotFoundExceptionWhenUpdateWorkByInvalidId() throws WorkNotFoundException, DateTimeFormatException, StatusNotFoundException {
        Task model = prepareTask();

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());

        workService.updateWork(model);
    }


    @Test
    public void shouldReturnIdWhenUpdateWorkSuccess() throws WorkNotFoundException, DateTimeFormatException, StatusNotFoundException {
        Task model = prepareTask();
        Work expectedResult = prepareWork();

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.of(expectedResult));

        String actualResult = workService.updateWork(model);

        Assertions.assertThat(actualResult).isNotBlank();
        Assertions.assertThat(actualResult).isEqualTo(expectedResult.getId());
    }


    @Test(expected = WorkNotFoundException.class)
    public void shouldThrowWorkNotFoundExceptionWhenDeleteWorkByInvalidId() throws Exception {
        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.empty());

        workService.deleteWork(Mockito.anyString());
    }


    @Test
    public void shouldReturnIdWhenDeleteWorkSuccess() throws Exception {
        Work expectedResult = prepareWork();
        ArgumentCaptor<Work> workArgumentCaptor = ArgumentCaptor.forClass(Work.class);

        Mockito.when(workRepository.findById(Mockito.anyString())).thenReturn(Optional.of(expectedResult));

        String actualResult = workService.deleteWork(Mockito.anyString());

        verify(workRepository, Mockito.times(1)).delete(workArgumentCaptor.capture());
        Assertions.assertThat(actualResult).isNotBlank();
        Assertions.assertThat(actualResult).isEqualTo(expectedResult.getId());
    }


    @DataProvider
    public static Object[] provideDataForFetchAllWorkErrorCases() {
        /*
         *
         * by default, constructor instance of AbstractPageRequest
         * with page < 0  or size < 1 will throw an IllegalArgumentException
         *
         * */
        int pageLessThanZero = -1;
        int pageGreaterThanOrEqualZero = 10;
        int sizeLessThanOne = 0;
        int sizeGreaterThanOrEqualOne = 1;

        return new Object[] [] {
                {pageLessThanZero, sizeGreaterThanOrEqualOne},
                {pageGreaterThanOrEqualZero, sizeLessThanOne},
                {pageLessThanZero, sizeLessThanOne},
        };
    }


    @Test(expected = PaginationSortingException.class)
    @UseDataProvider(value = "provideDataForFetchAllWorkErrorCases")
    public void shouldThrowPaginationSortingExceptionWhenFetchAllWork(int page, int size) throws PaginationSortingException {
        workService.fetchAllWork(page, size);
    }


    @Test
    public void shouldReturnPageOfWorkWhenFetchAllWorkSuccess() throws PaginationSortingException {
        Page<Work> expectedResult = prepareWorkPage();

        Mockito.when(workRepository.findAll(Mockito.any(Pageable.class))).thenReturn(expectedResult);

        Page<Work> actualResult = workService.fetchAllWork(0, expectedResult.getSize());

        Assertions.assertThat(actualResult).isNotEmpty();
        Assertions.assertThat(actualResult.getTotalElements()).isEqualTo(expectedResult.getTotalElements());
        Assertions.assertThat(actualResult.getTotalPages()).isEqualTo(expectedResult.getTotalPages());
        Assertions.assertThat(actualResult.getContent()).hasSize(expectedResult.getContent().size());
        Assertions.assertThat(actualResult.getContent()).extracting(Work::getId).isEqualTo(getWorkIds(expectedResult));
    }


    private List<String> getWorkIds(Page<Work> page) {
        return page.getContent().stream()
                .map(Work::getId)
                .collect(Collectors.toList());
    }


    @DataProvider
    public static Object[] provideDataForFetchAllWorkByOrderErrorCases() {
        /*
         *
         * by default, constructor instance of AbstractPageRequest
         * with page < 0  or size < 1 will throw an IllegalArgumentException
         *
         * */
        int pageLessThanZero = -1;
        int pageGreaterThanOrEqualZero = 10;
        int sizeLessThanOne = 0;
        int sizeGreaterThanOrEqualOne = 1;
        String validDirection = Direction.ASCENDING.getValue();
        String invalidDirection = "Wrong direction";
        String validOrderBy = "workName";
        String invalidOrderBy = "Invalid workName";

        return new Object[] [] {
                {pageLessThanZero, sizeGreaterThanOrEqualOne, validOrderBy, validDirection},
                {pageLessThanZero, sizeLessThanOne, invalidOrderBy, invalidDirection},
                {pageLessThanZero, sizeLessThanOne, invalidOrderBy, validDirection},
                {pageLessThanZero, sizeLessThanOne, validOrderBy, invalidDirection},
                {pageLessThanZero, sizeLessThanOne, validOrderBy, validDirection},
                {pageLessThanZero, sizeGreaterThanOrEqualOne, invalidOrderBy, invalidDirection},
                {pageLessThanZero, sizeGreaterThanOrEqualOne, invalidOrderBy, validDirection},
                {pageLessThanZero, sizeGreaterThanOrEqualOne, validOrderBy, invalidDirection},
                {pageGreaterThanOrEqualZero, sizeLessThanOne, validOrderBy, validDirection},
                {pageGreaterThanOrEqualZero, sizeGreaterThanOrEqualOne, invalidOrderBy, validDirection},
                {pageGreaterThanOrEqualZero, sizeGreaterThanOrEqualOne, validOrderBy, invalidDirection},
                {pageGreaterThanOrEqualZero, sizeLessThanOne, invalidOrderBy, invalidDirection},
                {pageGreaterThanOrEqualZero, sizeLessThanOne, invalidOrderBy, validDirection},
                {pageGreaterThanOrEqualZero, sizeLessThanOne, validOrderBy, invalidDirection},
                {pageGreaterThanOrEqualZero, sizeGreaterThanOrEqualOne, invalidOrderBy, invalidDirection},
        };
    }


    @Test(expected = PaginationSortingException.class)
    @UseDataProvider(value = "provideDataForFetchAllWorkByOrderErrorCases")
    public void shouldThrowPaginationSortingExceptionWhenFetchAllWorkByOrder(int page,
                                                                             int size,
                                                                             String orderBy,
                                                                             String direction) throws PaginationSortingException {
        workService.fetchAllWork(page, size, orderBy, direction);
    }


    @DataProvider
    public static Object[] provideDataForFetchAllWorkByOrderSuccessCases() {
        String ascendingDirection = Direction.ASCENDING.getValue();
        String descendingDirection = Direction.DESCENDING.getValue();
        String orderByWorkName = OrderBy.WORK_NAME.getValue();
        String orderByStartingDate = OrderBy.STARTING_DATE.getValue();

        return new Object[] [] {
                {ascendingDirection, orderByWorkName},
                {descendingDirection, orderByStartingDate}
        };
    }


    @Test
    @UseDataProvider(value = "provideDataForFetchAllWorkByOrderSuccessCases")
    public void shouldReturnPageOfWorkWhenFetchAllWorkByOrderSuccess(String direction, String orderBy) throws PaginationSortingException {
        Page<Work> expectedResult = prepareWorkPage();

        Mockito.when(workRepository.findAll(Mockito.any(Pageable.class))).thenReturn(expectedResult);

        Page<Work> actualResult = workService.fetchAllWork(0, expectedResult.getContent().size(),direction, orderBy);

        Assertions.assertThat(actualResult).isNotEmpty();
        Assertions.assertThat(actualResult.getTotalElements()).isEqualTo(expectedResult.getTotalElements());
        Assertions.assertThat(actualResult.getTotalPages()).isEqualTo(expectedResult.getTotalPages());
        Assertions.assertThat(actualResult.getContent()).hasSize(expectedResult.getContent().size());
        Assertions.assertThat(actualResult.getContent()).extracting(Work::getId).isEqualTo(getWorkIds(expectedResult));
    }


    public Work prepareWork() {
        return testDataLoader.loadInstance(workFileName, Work.class);
    }


    public Task prepareTask() {
        return testDataLoader.loadInstance(taskFileName, Task.class);
    }


    public Page<Work> prepareWorkPage() {
        return testDataLoader.loadInstance(workPageFile, new TypeReference<Page<Work>>() {});
    }
}