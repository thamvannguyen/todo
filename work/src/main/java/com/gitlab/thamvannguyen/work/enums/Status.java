package com.gitlab.thamvannguyen.work.enums;

import lombok.Getter;

/**
 *
 * Enums specify status of <i>Work</i> class
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Getter
public enum Status {
    PLANNING("Planning"),
    DOING("Doing"),
    COMPLETE("Complete");

    private String value;

    Status(String orderBy) {
        this.value = orderBy;
    }
}
