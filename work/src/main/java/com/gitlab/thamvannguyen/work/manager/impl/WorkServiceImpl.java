package com.gitlab.thamvannguyen.work.manager.impl;

import com.gitlab.thamvannguyen.core.util.datetime.DateTimeUtil;
import com.gitlab.thamvannguyen.work.constants.Constants;
import com.gitlab.thamvannguyen.work.dao.entity.Work;
import com.gitlab.thamvannguyen.work.dao.repository.WorkRepository;
import com.gitlab.thamvannguyen.work.dto.Task;
import com.gitlab.thamvannguyen.work.enums.Status;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import com.gitlab.thamvannguyen.work.manager.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeParseException;
import java.util.Optional;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Service
public class WorkServiceImpl implements WorkService {

    @Autowired
    private WorkRepository workRepository;

    /**
     * Create work
     *
     * @param model the object carries on data
     * @return an instance of Work class
     * @throws StatusNotFoundException when the status is invalid
     * @throws DateTimeFormatException when datetime wrong format
     */
    @Override
    public Work createWork(Task model) throws StatusNotFoundException, DateTimeFormatException {
        return workRepository.save(convertModelToWork(model));
    }


    /**
     * Get work by Id
     *
     * @param id id of work
     * @return an instance of Work class
     * @throws WorkNotFoundException when Work not found by given id
     */
    @Override
    public Work getWork(String id) throws WorkNotFoundException {
        Optional<Work> workOptional = workRepository.findById(id);
        if(workOptional.isPresent()) {
            return workOptional.get();
        }
        throw new WorkNotFoundException();
    }


    /**
     * Update work
     *
     * @param task the object carries on data
     * @return id of the updated object
     * @throws WorkNotFoundException when work not found in database
     * @throws StatusNotFoundException when the status is invalid
     * @throws DateTimeFormatException when datetime wrong format
     */
    @Override
    public String updateWork(Task task) throws WorkNotFoundException, StatusNotFoundException, DateTimeFormatException {
        try {
            Work work = getWork(task.getId().trim());
            work.setWorkName(task.getWorkName().trim());
            work.setStartingDate(DateTimeUtil.convertStringToLocalDateTime(task.getStartingDate().trim()));
            work.setEndingDate(DateTimeUtil.convertStringToLocalDateTime(task.getEndingDate().trim()));
            work.setStatus(Enum.valueOf(Status.class, task.getStatus().trim()));
            workRepository.saveAndFlush(work);
            return work.getId();
        } catch (Exception exception) {
            if (exception instanceof DateTimeParseException)
                throw new DateTimeFormatException(exception);

            if (exception instanceof IllegalArgumentException) {
                throw new StatusNotFoundException(exception);
            }

            throw exception;
        }
    }


    /**
     * Delete work by id
     *
     * @param id id of work
     * @return id of deleted work
     * @throws WorkNotFoundException when work not found in database
     */
    @Override
    public String deleteWork(String id) throws WorkNotFoundException {
        Optional<Work> workOptional = workRepository.findById(id);
        if(workOptional.isPresent()) {
            Work work = workOptional.get();
            workRepository.delete(work);
            return work.getId();
        }
        throw new WorkNotFoundException();
    }


    /**
     * Fetch all work by page and size
     *
     * @param page index of page
     * @param size number of work
     * @return a Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    @Override
    public Page<Work> fetchAllWork(int page, int size) throws PaginationSortingException {
        try {
            Pageable pageable = PageRequest.of(page, size, Sort.by(Constants.WORK_NAME).ascending());
            return workRepository.findAll(pageable);
        }catch (IllegalArgumentException exception) {
            throw new PaginationSortingException(exception);
        }
    }


    /**
     * Fetch all work by page, size, direction and order by
     * @param page index of page
     * @param size number of work
     * @param direction direction of sorting
     * @param orderBy order by field
     * @return Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    @Override
    public Page<Work> fetchAllWork(int page, int size, String direction, String orderBy) throws PaginationSortingException {
        try {
            Sort.Direction directionType = getDirection(direction.toUpperCase());
            Pageable pageable = PageRequest.of(page, size, directionType, orderBy);
            return workRepository.findAll(pageable);
        }catch (Exception exception) {
            throw new PaginationSortingException(exception);
        }
    }


    private Sort.Direction getDirection(String direction) throws PaginationSortingException {
        if (Sort.Direction.ASC.name().equals(direction)) {
            return Sort.Direction.ASC;
        }else if (Sort.Direction.DESC.name().equals(direction)) {
            return Sort.Direction.DESC;
        }
        throw new PaginationSortingException();
    }


    private Work convertModelToWork(Task task) throws StatusNotFoundException, DateTimeFormatException {
        try {
            Work work = new Work();
            work.setWorkName(task.getWorkName().trim());
            work.setStartingDate(DateTimeUtil.convertStringToLocalDateTime(task.getStartingDate().trim()));
            work.setEndingDate(DateTimeUtil.convertStringToLocalDateTime(task.getEndingDate().trim()));
            work.setStatus(Enum.valueOf(Status.class, task.getStatus().trim().toUpperCase()));
            return work;
        } catch (Exception exception) {
            if(exception instanceof DateTimeParseException) {
                throw new DateTimeFormatException(exception);
            }

            throw new StatusNotFoundException(exception);
        }
    }
}
