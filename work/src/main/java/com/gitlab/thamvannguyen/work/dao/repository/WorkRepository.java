package com.gitlab.thamvannguyen.work.dao.repository;

import com.gitlab.thamvannguyen.core.jpa.repository.AbstractRepository;
import com.gitlab.thamvannguyen.work.dao.entity.Work;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Repository
public interface WorkRepository extends AbstractRepository<Work, String> {
}
