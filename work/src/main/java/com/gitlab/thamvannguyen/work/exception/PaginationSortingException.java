package com.gitlab.thamvannguyen.work.exception;

import com.gitlab.thamvannguyen.core.exception.BusinessException;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class PaginationSortingException extends BusinessException {

    private static final long serialVersionUID = -5679312647466427364L;

    public PaginationSortingException() {
        super();
    }

    public PaginationSortingException(Exception exception) {
        super(exception);
    }
}
