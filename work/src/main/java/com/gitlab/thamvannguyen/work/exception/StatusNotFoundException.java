package com.gitlab.thamvannguyen.work.exception;

import com.gitlab.thamvannguyen.core.exception.BusinessException;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class StatusNotFoundException extends BusinessException {

    private static final long serialVersionUID = 3047320353401958099L;

    public StatusNotFoundException(Exception exception) {
        super(exception);
    }
}
