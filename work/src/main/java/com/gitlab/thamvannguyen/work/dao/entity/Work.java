package com.gitlab.thamvannguyen.work.dao.entity;

import com.gitlab.thamvannguyen.core.jpa.auditing.AbstractAuditableEntity;
import com.gitlab.thamvannguyen.work.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 *
 * This class represent a work,
 * each record in the work table will map an instance of this class.
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "work")
public class Work extends AbstractAuditableEntity<String> {

    private static final long serialVersionUID = 5719565290080523011L;

    @Column(name = "work_name")
    private String workName;

    @Column(name = "starting_date")
    private LocalDateTime startingDate;

    @Column(name = "ending_date")
    private LocalDateTime endingDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    public Work() {
        this.setId(UUID.randomUUID().toString());
    }
}
