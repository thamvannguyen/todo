package com.gitlab.thamvannguyen.work.exception;

import com.gitlab.thamvannguyen.core.exception.BusinessException;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class WorkNotFoundException extends BusinessException {

    private static final long serialVersionUID = -1185270165187270630L;
}
