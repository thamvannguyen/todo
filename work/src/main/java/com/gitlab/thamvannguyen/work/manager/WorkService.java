package com.gitlab.thamvannguyen.work.manager;

import com.gitlab.thamvannguyen.work.dao.entity.Work;
import com.gitlab.thamvannguyen.work.dto.Task;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import org.springframework.data.domain.Page;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public interface WorkService {

    /**
     *
     * Create work
     *
     * @param model the object carries on data
     * @return an instance of Work class
     * @throws StatusNotFoundException when the status is invalid
     * @throws DateTimeFormatException when datetime wrong format
     */
    Work createWork(Task model) throws StatusNotFoundException, DateTimeFormatException;

    /**
     * Get work by Id
     *
     * @param id id of work
     * @return an instance of Work class
     * @throws WorkNotFoundException when Work not found by given id
     */
    Work getWork(String id) throws WorkNotFoundException;

    /**
     * Update work
     *
     * @param model the object carries on data
     * @return id of the updated object
     * @throws WorkNotFoundException when work not found in database
     * @throws StatusNotFoundException when the status is invalid
     * @throws DateTimeFormatException when datetime wrong format
     */
    String updateWork(Task model) throws WorkNotFoundException, DateTimeFormatException, StatusNotFoundException;

    /**
     * Delete work by id
     *
     * @param id id of work
     * @return id of deleted work
     * @throws WorkNotFoundException when work not found in database
     */
    String deleteWork(String id) throws WorkNotFoundException;

    /**
     * Fetch all work by page and size
     *
     * @param page index of page
     * @param size number of work
     * @return a Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    Page<Work> fetchAllWork(int page, int size) throws PaginationSortingException;

    /**
     * Fetch all work by page, size, direction and order by
     * @param page index of page
     * @param size number of work
     * @param direction direction of sorting
     * @param orderBy order by field
     * @return Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    Page<Work> fetchAllWork(int page, int size, String orderBy, String direction) throws PaginationSortingException;
}
