package com.gitlab.thamvannguyen.work.enums;

import lombok.Getter;

/**
 *
 * Enums for verifying orderBy param of paging API
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Getter
public enum OrderBy {
    WORK_NAME("workName"),
    STARTING_DATE("startingDate"),
    ENDING_DATE("endingDate"),
    STATUS("status");

    private String value;

    OrderBy(String orderBy) {
        this.value = orderBy;
    }
}
