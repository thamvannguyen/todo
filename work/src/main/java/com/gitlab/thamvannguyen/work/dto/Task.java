package com.gitlab.thamvannguyen.work.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 *
 * This class used to encapsulate data in a value object that could be transferred
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Getter
@Setter
@EqualsAndHashCode
public class Task {
    private String id;
    @NotBlank
    private String workName;
    @NotBlank
    private String startingDate;
    @NotBlank
    private String endingDate;
    @NotBlank
    private String status;
}