package com.gitlab.thamvannguyen.work.enums;

import lombok.Getter;

/**
 *
 * Enums for verifying direction(ascending, descending) param of paging API
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Getter
public enum Direction {
    ASCENDING("ASC"),
    DESCENDING("DESC");

    private String value;

    Direction(String direction) {
        this.value = direction;
    }
}
