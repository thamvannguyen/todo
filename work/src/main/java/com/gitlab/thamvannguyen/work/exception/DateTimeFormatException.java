package com.gitlab.thamvannguyen.work.exception;

import com.gitlab.thamvannguyen.core.exception.BusinessException;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class DateTimeFormatException extends BusinessException {

    private static final long serialVersionUID = -8257394364641163942L;

    public DateTimeFormatException(Exception exception) {
        super(exception);
    }
}
