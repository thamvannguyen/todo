package com.gitlab.thamvannguyen.core.exception;

/**
 *
 * This Exception defines a common hood for all the business Exceptions.
 * All exceptions delivered to client should inherit from this class.
 * <p>
 * A corresponding error code and message should be defined in
 * <tt>CommsMessage.properties</tt> for inherited class.
 * <p>
 * e.g. For defining <i>MyException</i>, following should be done.
 * <p>
 * <code>
 * package com.mypackage;
 * <p>
 * public class MyException extends BusinessException {
 *   <p>
 *   MyException(){}
 *   <p>
 *   ...
 * }
 * </code>
 * <p>
 * Entry for <tt>CommsMessage.properties</tt>,
 * <p>
 * <code>
 * com.mypackage.MyException.code = 89001
 * <br>
 * com.mypackage.MyException.message = appropriate error message
 * </code>
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class BusinessException extends Exception {

    private static final long serialVersionUID = -4411973404024424747L;

    private Object param=null;

    /**
     * This is a deprecated constructor. Just to support the existing
     * framework of exception handling
     */
    public BusinessException() {
        super();
    }

    /**
     * Business-Tier should use this constructor whenever a business
     * violation occurs.
     * @param errorName a message that is used for detailing of an error.
     * @param rootException This might be a instance of Business
     *                  Exception or Any of the Technical Exception.
     */
    public BusinessException(String errorName, Exception rootException){
        super(errorName,rootException);
    }

    /**
     * Business-Tier should use this constructor whenever a business
     * violation occurs. This constructor will be useful if you want to thrown
     * any exception that is not caught.
     * @param errorName errorMessage a message that is used for detailing of
     *                     an error.
     */

    public BusinessException(String errorName) {
        super(errorName);
    }
    /**
     * Business-Tier should use this constructor whenever a business
     * violation occurs.
     * @param exception This might be a instance of Business
     *                  Exception or Any of the Technical Exception.
     */
    public BusinessException(Exception exception){
        super(exception);
    }

    /**
     *
     * Method returns a message associated with the Error condition.
     * @return : {@link String} error message
     */
    public String getErrorName() {
        return getMessage();
    }

    /**
     *
     * Method returns the root cause for this Error condition
     * @return : {@link Throwable} Root Exception
     */
    public Throwable getRootException() {
        return getCause();
    }

    /**
     * This constructor passes throwable instance to super class
     *
     * @param cause : exception caused
     */
    public BusinessException(Throwable cause) {
        super(cause);
    }

    /**
     * This constructor passes throwable instance and message to super class
     * @param message : exception message.
     * @param cause : exception cause.
     */
    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param param The param to set.
     */
    public void setParam(Object param) {
        this.param = param;
    }

    /**
     * @return Returns the param.
     */
    public Object getParam() {
        return param;
    }
}
