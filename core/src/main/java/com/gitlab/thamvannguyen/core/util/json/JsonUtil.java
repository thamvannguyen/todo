package com.gitlab.thamvannguyen.core.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonTokenId;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.gitlab.thamvannguyen.core.util.datetime.DateTimeUtil.DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS;

/**
 * This class provides some functions working with JSON type,
 * allowing the convert JSON to Object and otherwise.
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class JsonUtil {

    private static ObjectMapper jsonObjectMapper;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS);

    static {
        jsonObjectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(LocalDateTime.class, new DateDeserializers());
        simpleModule.addSerializer(new LocalDateTimeSerializer());
        jsonObjectMapper.registerModule(simpleModule);
        jsonObjectMapper.registerModule(new PageModule());
    }


    /**
     *
     * Convert JSON to Object with the class name
     *
     * @param json an input string
     * @param className name of the class which needs to convert
     * @return an instance of className
     */
    public static <T> T convertJsonToObject(String json, Class<T> className) {
        try {
            return jsonObjectMapper.readValue(json, className);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    /**
     *
     * Convert JSON to Object with the reference type
     *
     * @param json an input string
     * @param typeReference type reference which needs to convert
     * @return an instance of className
     */
    public static <T> T convertJsonToObject(String json, TypeReference<T> typeReference) {
        try {
            return jsonObjectMapper.readValue(json, typeReference);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    /**
     *
     * Convert Object to JSON
     *
     * @param object the object which needs convert to JSON
     * @return a JSON string
     */
    public static String convertObjectToJson(Object object) {
        try {
            return jsonObjectMapper.writeValueAsString(object);
        }catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }


    private static class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

        private static final long serialVersionUID = -8889982147286480326L;

        LocalDateTimeSerializer() {
            this(LocalDateTime.class);
        }

        LocalDateTimeSerializer(Class<LocalDateTime> t) {
            super(t);
        }

        @Override
        public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

            jsonGenerator.writeString(localDateTime.format(dateTimeFormatter));
        }
    }


    private static class DateDeserializers extends StdDeserializer<LocalDateTime> {
        private static final long serialVersionUID = -745580251313634254L;

        DateDeserializers() {
            this(null);
        }

        DateDeserializers(Class<?> vc) {
            super(vc);
        }

        @Override
        public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return LocalDateTime.parse(jsonParser.getText(), dateTimeFormatter);
        }
    }


    private static class PageModule extends SimpleModule {


        private static final long serialVersionUID = -1014455634158325208L;

        public PageModule() {
            addDeserializer(Page.class, new PageDeserializer());
        }
    }

    private static class PageDeserializer extends JsonDeserializer<Page<?>> implements ContextualDeserializer {
        private static final String CONTENT = "content";
        private static final String NUMBER = "number";
        private static final String SIZE = "size";
        private static final String TOTAL_ELEMENTS = "totalElements";
        private JavaType valueType;

        @Override
        public Page<?> deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
            long total = 0;
            int pageSize = 0;
            int pageNumber = 0;
            List<?> content = new ArrayList<>();
            final CollectionType type = context.getTypeFactory().constructCollectionType(List.class, valueType);

            if (jsonParser.isExpectedStartObjectToken()) {
                jsonParser.nextToken();
                if (jsonParser.hasTokenId(JsonTokenId.ID_FIELD_NAME)) {
                    String propName = jsonParser.getCurrentName();
                    do {
                        jsonParser.nextToken();
                        switch (propName) {
                            case CONTENT:
                                content = context.readValue(jsonParser, type);
                                break;
                            case NUMBER:
                                pageNumber = context.readValue(jsonParser, Integer.class);
                                break;
                            case SIZE:
                                pageSize = context.readValue(jsonParser, Integer.class);
                                break;
                            case TOTAL_ELEMENTS:
                                total = context.readValue(jsonParser, Long.class);
                                break;
                            default:
                                jsonParser.skipChildren();
                                break;
                        }
                    } while (((propName = jsonParser.nextFieldName())) != null);
                } else {
                    context.handleUnexpectedToken(handledType(), jsonParser);
                }
            } else {
                context.handleUnexpectedToken(handledType(), jsonParser);
            }

            return new PageImpl<>(content, PageRequest.of(pageNumber, pageSize), total);
        }


        /**
         * This is the main point here.
         * The PageDeserializer is created for each specific deserialization
         * with concrete generic parameter type of Page.
         */
        @Override
        public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) {
            //This is the Page actually
            final JavaType wrapperType = ctxt.getContextualType();
            final PageDeserializer deserializer = new PageDeserializer();
            //This is the parameter of Page
            deserializer.valueType = wrapperType.containedType(0);
            return deserializer;
        }
    }
}