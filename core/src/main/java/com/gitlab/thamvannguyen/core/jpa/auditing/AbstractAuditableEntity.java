package com.gitlab.thamvannguyen.core.jpa.auditing;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * This class entity can be used to trigger capturing auditing information,
 * Provides sophisticated support to transparently keep track of who created
 * or changed an entity and the point in time this happened.
 * <p>
 * e.g. For track object <i>Work</i>, following should be done.
 * </p>
 * <p>
 * <code>
 * package com.gitlab.thamvannguyen.work.dao.entity;
 * public class Work extends AbstractAuditableEntity<String> {
 *   private static final long serialVersionUID = 5719565290080523011L;
 *   <p>
 *   public Work(){}
 *   </p>
 *   ...
 * }
 * </code>
 * </p>
 *
 * @param <PK> primary key of entity
 * @note <PK> must must implement Serializable interface
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@ToString
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AbstractAuditableEntity <PK extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1603841156447422985L;

    @Id
    private PK id;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @LastModifiedDate
    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @CreatedBy
    @Column(name = "created_by")

    private String createBy;

    @LastModifiedBy
    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Version
    @Column(name = "version_no")
    private Integer versionNo;

    @Column(name = "deleted")
    private boolean deleted;
}
