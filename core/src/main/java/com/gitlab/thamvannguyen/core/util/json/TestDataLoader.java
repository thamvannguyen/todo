package com.gitlab.thamvannguyen.core.util.json;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.Builder;
import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;

/**
 *
 * This class provides some functions working with JSON type,
 * allowing the convert JSON to Object from file
 * .
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Builder
public class TestDataLoader {
    private String path;

    /**
     *
     * Get instance of class from file name and class name
     *
     * @param fileName name of file
     * @param className the class name of instance which needs create
     * @return an instance of class name
     */
    public <T> T loadInstance(String fileName, Class<T> className) {
        try {
            return JsonUtil.convertJsonToObject(FileUtils.readFileToString(ResourceUtils.getFile(TestDataLoader.class.getResource(path.concat(fileName)))), className);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     *
     * Get instance of class from file name and type reference
     *
     * @param fileName name of file
     * @param typeReference type reference which needs create
     * @return an instance of reference type
     */
    public <T> T loadInstance(String fileName, TypeReference<T> typeReference) {
        try {
            return JsonUtil.convertJsonToObject(FileUtils.readFileToString(ResourceUtils.getFile(TestDataLoader.class.getResource(path.concat(fileName)))), typeReference);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
