package com.gitlab.thamvannguyen.core.util.datetime;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * This class provides some functions working with Date time type,
 * such as allowing the formatting, parsing, convert of date strings
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class DateTimeUtil {
    public static final String YEAR_MONTH_DAY_TIME_FULL_PATTERN_WITH_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ssZZ";
    public static final String YEAR_MONTH_DAY_TIME_FULL_PATTERN_WITHOUT_SPECIAL_CHARACTERS = "yyyyMMddHHmmss";
    public static final String YEAR_MONTH_DAY_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS = "yyyy-MM-dd HH:mm:ss";
    public static final String MONTH_DAY_YEAR_TIME_FULL_PATTERN_WITHOUT_SPECIAL_CHARACTERS = "MMddyyyyHHmmss";
    public static final String MONTH_DAY_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS = "MM-dd-yyyy HH:mm:ss";
    public static final String DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITHOUT_SPECIAL_CHARACTERS = "ddMMyyyyHHmmss";
    public static final String DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS = "yyyy-MM-dd HH:mm:ss";

    /**
     *
     * Convert a string datetime from
     * a timezone to another timezone by input/output format
     *
     * @param inputDateTime date string which needs convert
     * @param inputDateTimeFormat the current format of the input
     * @param fromTimeZone the current timezone of the input
     * @param toTimeZone the output timezone
     * @param outputDateTimeFormat the output format of datetime string
     * @return  a datetime string with output format in toTimeZone.
     */
    public static String convertTimeZone(String inputDateTime, String inputDateTimeFormat, String fromTimeZone, String toTimeZone,
                                         String outputDateTimeFormat) {

        Calendar fromDate = getInstanceOfCalendarWithTimeZone(inputDateTime, inputDateTimeFormat, fromTimeZone);
        return convertTimeZone(fromDate, toTimeZone, outputDateTimeFormat);
    }

    /**
     *
     * Convert a date to timezone and output format
     *
     * @param fromDate an instance of Calendar class which needs convert
     * @param toTimeZone timeZone which needs the format
     * @param outputDateTimeFormat output format
     * @return a datetime string with output format in toTimeZone.
     */
    public static String convertTimeZone(Calendar fromDate, String toTimeZone, String outputDateTimeFormat) {
        return formatDateToTimeZone(fromDate.getTime(), toTimeZone, outputDateTimeFormat);
    }

    /**
     *
     * Convert a date to timezone and output format
     *
     * @param fromDate an instance of Date class which needs convert
     * @param toTimeZone timeZone which needs the format
     * @param outputDateTimeFormat output format
     * @return a datetime string with output format in toTimeZone.
     */
    public static String formatDateToTimeZone(Date fromDate, String toTimeZone, String outputDateTimeFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(outputDateTimeFormat);
        dateFormat.setTimeZone(TimeZone.getTimeZone(toTimeZone));
        return dateFormat.format(fromDate.getTime());
    }

    /**
     *
     * Convert a string to LocalDateTime type with yyyy-MM-dd HH:mm:ss format
     *
     * @param input a string which needs convert
     * @return an instance of LocalDateTime class
     */
    public static LocalDateTime convertStringToLocalDateTime(String input)  {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS);
        return LocalDateTime.parse(input, formatter);
    }

    /**
     *
     * Convert a LocalDateTime object to string with yyyy-MM-dd HH:mm:ss format
     *
     * @param dateTime an instance of LocalDateTime class which needs convert
     * @return a date time string with format yyyy-MM-dd HH:mm:ss
     */
    public static String convertLocalDateTimeToString(LocalDateTime dateTime)  {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_TIME_FULL_PATTERN_WITH_SPECIAL_CHARACTERS);
        return dateTime.format(formatter);
    }

    /**
     *
     * Get an instance of Calendar from date string and time zone
     *
     * @param dateInputValue a date string value
     * @param dateInputFormat a format which needs format a date string value
     * @param timeZone
     * @return an instance of Calendar class
     */
    public static Calendar getInstanceOfCalendarWithTimeZone(String dateInputValue, String dateInputFormat, String timeZone) {
        Calendar calendarInstance = Calendar.getInstance();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateInputFormat);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            calendarInstance.setTime(dateFormat.parse(dateInputValue));
        } catch (ParseException e) {
            throw new IllegalArgumentException(
                    String.format("Input value is invalid: inputDateTime=%s, inputDateTimeFormat=%s, timeZone=%s", dateInputValue, dateInputFormat, timeZone));
        }
        return calendarInstance;
    }

    /**
     *
     * Get an instance of Calendar from date string
     *
     * @param dateInputValue a date string value
     * @param dateInputFormat a format which needs format a date string value
     * @return an instance of Calendar class
     */
    public static Calendar getInstanceOfCalendar(String dateInputValue, String dateInputFormat) {
        Calendar calendarInstance = Calendar.getInstance();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateInputFormat);
            calendarInstance.setTime(dateFormat.parse(dateInputValue));
        } catch (ParseException e) {
            throw new IllegalArgumentException(
                    String.format("Input value is invalid: inputDateTime=%s, inputDateTimeFormat=%s", dateInputValue, dateInputFormat));
        }
        return calendarInstance;
    }

    /**
     *
     * Get timezone from a client timestamp string
     *
     * @param clientTimestamp a timestamp string which needs get time zone
     * @return the time zone of an input string
     */
    public static String getTimezone(String clientTimestamp) {
        String timezone = StringUtils.EMPTY;
        String patternStr = "((\\+|\\-)(\\d|:){4,5})";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(clientTimestamp);
        if (matcher.find()) {
            timezone = matcher.group();
        }
        return timezone;
    }

    /**
     * Get interval day between old and new Date
     *
     * @param newDate
     * @param oldDate
     * @return number of days
     */
    public static int getDiffInDay(Date newDate, Date oldDate) {
        return Math.round((float) ((newDate.getTime() - oldDate.getTime()) / 86400000L));
    }

    /**
     *
     * Get interval minute between old and new Date
     *
     * @param newDate
     * @param oldDate
     * @return number of minutes
     */
    public static int getDiffInMinute(Date newDate, Date oldDate) {
        return Math.round((float) ((newDate.getTime() - oldDate.getTime()) / 60000L));
    }

    /**
     *
     * Convert a Date object to string with format
     *
     * @param date a date object which needs convert
     * @param format an output format
     * @return string with output format
     */
    public static String convertDateToString(Date date, String format) {
        if (date != null && !date.toString().equals("")) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(date);
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     *
     * Compare two date objects
     *
     * @param startDate
     * @param endDate
     * @param dateIsEqual flag specify condition compare equal startDate and endDate
     * @return boolean result of compare two date object
     */
    public static boolean isValidDateRange(Date startDate, Date endDate, boolean dateIsEqual) {
        if (startDate == null || endDate == null) {
            return false;
        }

        if (dateIsEqual) {
            if (startDate.equals(endDate)) {
                return true;
            }
        }

        if (endDate.after(startDate)) {
            return true;
        }

        return false;
    }

    /**
     *
     * Get GmtServerTimezone
     *
     * @return a GmtServerTimezone string
     */
    public static String getGmtServerTimezone() {
        int gmtOffset = Calendar.getInstance().getTimeZone().getRawOffset();
        long hours = TimeUnit.MILLISECONDS.toHours(gmtOffset);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(gmtOffset) - TimeUnit.HOURS.toMinutes(hours);
        if (hours > 0) {
            return String.format("GMT+%d%02d", hours, minutes);
        } else {
            return String.format("GMT%d%02d", hours, minutes);
        }
    }
}
