package com.gitlab.thamvannguyen.core.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;


/**
 *
 * This class defines an abstract repository
 * which is the contract of the domain layer with the persistence layer.
 *
 * For inheritance purpose, a concrete class just simple extends this class,
 * by the way providing  entity class name and primary key
 *
 * In addition, implement common query command in this class
 * help child class simple use it that not declare multiple time.
 * <p>
 * e.g Declare <i>WorkRepository</i>, following should be done.
 * </p>
 * <p>
 *     <code>
 * @Repository
 * public interface WorkRepository extends AbstractRepository<Work, String> {
 *     ...
 * }
 *     </code>
 * </p>
 *
 * @note ID must implement Serializable interface
 *
 * @param <T> entity class name
 * @param <ID> primary key type
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@NoRepositoryBean
public interface AbstractRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
    T findByIdAndDeletedIsFalse(ID id);

    @Query("SELECT e FROM #{#entityName} e WHERE e.deleted = false")
    List<T> findAllNotDeleted();
}