package com.gitlab.thamvannguyen.core.util.bean;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * This class used for bean lookup purpose and by implementing the ApplicationContextAware interface
 *
 * To be able to get the bean, by call method getBean with argument class name or string
 * <p>
 * e.g Get bean for <i>Work</i> by class name, following should be done.
 * <code>
 *     Work work = BeanUtil.getBean(Work.class);
 * </code>
 * </p>
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class BeanUtil implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanUtil.context = applicationContext;
    }

    /**
     *
     * Get bean through bean name
     *
     * @param beanName name of bean need get bean
     * @param <Bean> type of bean returned
     * @return bean that match with name
     */
    public static <Bean> Bean getBean(String beanName) {
        return (Bean) context.getBean(beanName);
    }

    /**
     *
     * Get bean through class name
     *
     * @param beanClass name of class need get bean
     * @param <Bean> type of bean returned
     * @return bean match with class name
     */
    public static <Bean> Bean getBean(Class<Bean> beanClass) {
        return (Bean) context.getBean(beanClass);
    }
}
