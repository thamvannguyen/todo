package com.gitlab.thamvannguyen.client.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

import static com.gitlab.thamvannguyen.client.constants.Constants.*;

/**
 *
 * This class provides some functions to get a message from the resource bundle,
 * depending on the locale that the corresponding message will be returned
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Component
public class TranslationUtil {

    private static ResourceBundleMessageSource messageSource;
    private static Locale locale =LocaleContextHolder.getLocale();

    @Autowired
    TranslationUtil(ResourceBundleMessageSource messageSource) {
        TranslationUtil.messageSource = messageSource;
    }

    /**
     *
     * Get the error code from resource bundle with parameter
     *
     * @param className as a key
     * @return the error code corresponding with key
     */
    public static String getErrorCode(String className) {
        return messageSource.getMessage(getCode(className, CODE_KEY), null, locale);
    }

    /**
     *
     * Get the error message from resource bundle with parameter
     *
     * @param className as a key
     * @return the error message corresponding with key
     */
    public static String getErrorMessage(String className) {
        return messageSource.getMessage(getCode(className, MESSAGE_KEY), null, locale);
    }

    private static String getCode(String className, String key) {
        return String.join(POINT, className, key);
    }
}
