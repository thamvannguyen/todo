package com.gitlab.thamvannguyen.client.exception;

import com.gitlab.thamvannguyen.core.exception.BusinessException;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class InvalidInputException extends BusinessException {
    private static final long serialVersionUID = -7400981735415028900L;
}
