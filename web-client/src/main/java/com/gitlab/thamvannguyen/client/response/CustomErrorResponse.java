package com.gitlab.thamvannguyen.client.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * This class carries on information returned to the client
 * when exception occurred
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomErrorResponse {
    private String error_code;
    private String error_message;
}
