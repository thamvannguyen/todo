package com.gitlab.thamvannguyen.client.controller;

import com.gitlab.thamvannguyen.client.exception.InvalidInputException;
import com.gitlab.thamvannguyen.work.dao.entity.Work;
import com.gitlab.thamvannguyen.work.dto.Task;
import com.gitlab.thamvannguyen.work.enums.Direction;
import com.gitlab.thamvannguyen.work.enums.OrderBy;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import com.gitlab.thamvannguyen.work.manager.WorkService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Validated
@RestController
@RequestMapping(value = "/todo/")
public class TodoController {

    @Autowired
    private WorkService workService;

    /**
     *
     * Create work
     *
     * @param model the object carries on data
     * @return an instance of Work class
     * @throws StatusNotFoundException when the status is invalid
     * @throws DateTimeFormatException when datetime wrong format
     */
    @PostMapping("create")
    public ResponseEntity<?> createWork(@com.gitlab.thamvannguyen.client.validator.Task @RequestBody Task model) throws StatusNotFoundException, DateTimeFormatException {
        Work work = workService.createWork(model);
        return ResponseEntity.status(HttpStatus.CREATED).body(work);
    }


    /**
     *
     * Update work
     *
     * @param model the object carries on data
     * @return id of the updated object
     * @throws WorkNotFoundException when work not found in database
     * @throws DateTimeFormatException when datetime wrong format
     * @throws InvalidInputException when input invalid
     * @throws StatusNotFoundException when the status is invalid
     */
    @PatchMapping("update")
    public ResponseEntity<?> updateWork(@com.gitlab.thamvannguyen.client.validator.Task @RequestBody Task model) throws WorkNotFoundException, DateTimeFormatException,
            InvalidInputException, StatusNotFoundException {
        /*check empty here for purpose id param with value ""*/
        if(StringUtils.isEmpty(model.getId())) {
            throw new InvalidInputException();
        }
        String id = workService.updateWork(model);
        return ResponseEntity.status(HttpStatus.OK).body(id);
    }


    /**
     *
     * Delete work by id
     *
     * @param id id of work
     * @return id of deleted work
     * @throws InvalidInputException when input is empty
     * @throws WorkNotFoundException when work not found in database
     */
    @DeleteMapping("delete")
    public ResponseEntity<?> deleteWork(@RequestParam @NotBlank String id) throws InvalidInputException, WorkNotFoundException {
        if(StringUtils.isEmpty(id)) {
            throw new InvalidInputException();
        }
        String deletedId = workService.deleteWork(id.trim());
        return ResponseEntity.status(HttpStatus.OK).body(deletedId);
    }


    /**
     *
     * Get work by Id
     *
     * @param id id of work
     * @return an instance of Work class
     * @throws WorkNotFoundException when work not found in database
     * @throws InvalidInputException when input invalid
     */
    @GetMapping("get")
    @ResponseBody
    public ResponseEntity<?> getWork(@RequestParam("id") @NotBlank String id) throws WorkNotFoundException, InvalidInputException {
        /*check empty here for purpose id param with value ""*/
        if(StringUtils.isEmpty(id)) {
            throw new InvalidInputException();
        }
        Work work = workService.getWork(id.trim());
        return ResponseEntity.status(HttpStatus.OK).body(work);
    }


    /**
     *
     * Fetch all work by page and size
     *
     * @param page index of page
     * @param size number of work
     * @return Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    @GetMapping("fetch")
    @ResponseBody
    public ResponseEntity<?> fetchAllWork(@RequestParam("page") int page, @RequestParam("size") int size) throws PaginationSortingException {
        Page<Work> workPage = workService.fetchAllWork(page, size);
        return ResponseEntity.status(HttpStatus.OK).body(workPage.getContent());
    }


    /**
     *
     * Fetch all work by page, size, direction and order by
     *
     * @param page index of page
     * @param size number of work
     * @param direction direction of sorting
     * @param orderBy order by field
     * @return Page of work
     * @throws PaginationSortingException when invalid page or size or both
     */
    @GetMapping(value = "fetch", params = {"orderBy", "direction", "page", "size"})
    @ResponseBody
    public ResponseEntity<?> fetchAllWork(@RequestParam("page") int page,
                                          @RequestParam("size") int size,
                                          @RequestParam("direction") String direction,
                                          @RequestParam("orderBy") String orderBy) throws PaginationSortingException {
        validateRequestParam(orderBy.trim(), direction.trim().toUpperCase());
        Page<Work> workPage = workService.fetchAllWork(page, size, direction, orderBy);
        return ResponseEntity.status(HttpStatus.OK).body(workPage.getContent());
    }


    /**
     *
     * Verify whether the params are a valid value
     *
     * @param orderBy order by field
     * @param direction direction of sorting
     * @throws PaginationSortingException when invalid page or size or both
     */
    private void validateRequestParam(@RequestParam("orderBy") String orderBy, @RequestParam("direction") String direction) throws PaginationSortingException {
        if (!Boolean.logicalAnd(isValidDirectionParam(direction), isValidOrderByParam(orderBy))) {
            throw new PaginationSortingException();
        }
    }

    private boolean isValidDirectionParam(@RequestParam("direction") String direction) {
        return Boolean.logicalOr(Direction.ASCENDING.getValue().equals(direction),
                Direction.DESCENDING.getValue().equals(direction));
    }

    private boolean isValidOrderByParam(String orderBy) {
        return BooleanUtils.or(new boolean[] {OrderBy.WORK_NAME.getValue().equals(orderBy),
                OrderBy.STARTING_DATE.getValue().equals(orderBy),
                OrderBy.ENDING_DATE.getValue().equals(orderBy),
                OrderBy.STATUS.getValue().equals(orderBy)});
    }
}
