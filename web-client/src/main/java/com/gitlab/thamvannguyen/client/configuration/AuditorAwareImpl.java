package com.gitlab.thamvannguyen.client.configuration;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

/**
 *
 * In case you use either @CreatedBy or @LastModifiedBy,
 * the auditing infrastructure somehow needs to become
 * aware of the current principal.
 *
 * So, we provide an AuditorAware interface
 * that implement to tell the infrastructure who
 * the current user or system interacting with the application is.
 *
 * The implementation is accessing the Authentication object
 * provided by Spring Security and looks up the custom UserDetails instance from it
 *
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = "SYSTEM";
        if (authentication != null && authentication.isAuthenticated()) {
            if (authentication.getPrincipal() instanceof String) {
                username = (String) authentication.getPrincipal();
            } else if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails userDetails = (UserDetails) authentication.getPrincipal();
                username = userDetails.getUsername();
            }
        }
        return Optional.of(username);
    }
}
