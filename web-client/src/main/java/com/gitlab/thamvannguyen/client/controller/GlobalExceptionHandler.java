package com.gitlab.thamvannguyen.client.controller;

import com.gitlab.thamvannguyen.client.exception.InvalidInputException;
import com.gitlab.thamvannguyen.client.response.CustomErrorResponse;
import com.gitlab.thamvannguyen.client.util.TranslationUtil;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

/**
 *
 * Handling errors correctly in APIs
 * while providing meaningful error messages is a very desirable feature,
 * as it can help the API client properly respond to issues.
 * The default behavior tends to be returning stack traces
 * that are hard to understand and ultimately useless for the API client.
 * Partitioning the error information into fields also enables the API client
 * to parse it and provide better error messages to the user.
 *
 * This class supports global Exception handler mechanism.
 * So we can implement the controller exception handling events
 * in a central location.
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(WorkNotFoundException.class)
    public ResponseEntity<CustomErrorResponse> handleWorkNotFoundException(Exception exception) {
        String className = exception.getClass().getName();
        CustomErrorResponse customErrorResponse = new CustomErrorResponse(TranslationUtil.getErrorCode(className),
                TranslationUtil.getErrorMessage(className));
        return new ResponseEntity<>(customErrorResponse, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({InvalidInputException.class,
            StatusNotFoundException.class,
            DateTimeFormatException.class,
            ConstraintViolationException.class,
            PaginationSortingException.class})
    public ResponseEntity<CustomErrorResponse> handleBadRequestException(Exception exception) {
        String className = exception.getClass().getName();
        CustomErrorResponse customErrorResponse = new CustomErrorResponse(TranslationUtil.getErrorCode(className),
                TranslationUtil.getErrorMessage(className));
        return new ResponseEntity<>(customErrorResponse, HttpStatus.BAD_REQUEST);
    }
}
