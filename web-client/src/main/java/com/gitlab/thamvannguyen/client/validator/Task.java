package com.gitlab.thamvannguyen.client.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static com.gitlab.thamvannguyen.client.constants.Constants.DEFAULT_VALIDATION_MESSAGE;

/**
 *
 * create a new @interface with the to define our annotation
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Target({ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TaskValidator.class)
@Documented
public @interface Task {
    String message() default DEFAULT_VALIDATION_MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
