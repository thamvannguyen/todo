package com.gitlab.thamvannguyen.client.constants;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class Constants {
    public static final String EN_LOCALE = "en";
    public static final String FR_LOCALE = "fr";
    public static final String POINT = ".";
    public static final String CODE_KEY = "code";
    public static final String MESSAGE_KEY = "message";
    public static final String BASE_NAME = "messages";
    public static final String DEFAULT_ENCODING = "UTF-8";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String DEFAULT_VALIDATION_MESSAGE = "Input is not correct format or null";
}
