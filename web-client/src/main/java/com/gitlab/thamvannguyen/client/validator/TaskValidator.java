package com.gitlab.thamvannguyen.client.validator;

import com.gitlab.thamvannguyen.client.exception.InvalidInputException;
import com.gitlab.thamvannguyen.core.util.datetime.DateTimeUtil;
import com.gitlab.thamvannguyen.core.util.log.LogUtil;
import com.gitlab.thamvannguyen.work.enums.Status;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

/**
 *
 * This class implements the logic to validate
 * a given constraint for a Task object
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class TaskValidator implements ConstraintValidator<Task, com.gitlab.thamvannguyen.work.dto.Task> {

    private static final Logger LOGGER = LogUtil.getLogger();

    @Override
    public boolean isValid(com.gitlab.thamvannguyen.work.dto.Task task, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if(isEmptyOrNullValue(task.getStartingDate())
            || isEmptyOrNullValue(task.getEndingDate())
            || isEmptyOrNullValue(task.getStatus())
            || isEmptyOrNullValue(task.getWorkName())) {
                throw new InvalidInputException();
            }

            return Boolean.logicalAnd(isValidDate(task.getStartingDate().trim(), task.getEndingDate().trim()),
                    isvalidStatus(task.getStatus().trim().toUpperCase()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }


    /**
     *
     * @param startDate starting date
     * @param endDate ending date
     * @return true if ending date equal or after starting date, otherwise return false
     */
    private boolean isValidDate(String startDate, String endDate) {
            LocalDateTime startingDate = DateTimeUtil.convertStringToLocalDateTime(startDate);
            LocalDateTime endingDate = DateTimeUtil.convertStringToLocalDateTime(endDate);
            return !endingDate.isBefore(startingDate);
    }


    /**
     *
     * @param status status of work
     * @return true when valid status, otherwise return false
     */
    private boolean isvalidStatus(String status) {
        try {
            Enum.valueOf(Status.class, status);
            return true;
        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }


    /**
     *
     * Verify whether the input string null or empty
     *
     * @note StringUtils.isEmpty(value) for case input empty request
     * @param value data send from client
     * @return true if value null or empty, otherwise return false
     */
    private boolean isEmptyOrNullValue(String value) {
        return Boolean.logicalOr(StringUtils.isEmpty(value), StringUtils.isEmpty(value.trim()));
    }
}
