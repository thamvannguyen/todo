package com.gitlab.thamvannguyen.client.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 *
 * When Spring Security is on the classpath, the auto-configuration secures all endpoints by default.
 * However, when it comes to complex applications,
 * we need different security policies per endpoints.
 * We also need to configure which endpoints should be secured,
 * what type of users should be able to access the endpoints,
 * and which endpoints should be public.
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/todo/**");
    }
}
