package com.gitlab.thamvannguyen.client.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.gitlab.thamvannguyen.client.constants.Constants.*;

/**
 *
 * This class implementation that simply uses the primary locale
 * specified in the "accept-language" header of the HTTP request(
 * that is, the locale sent by the client browser, normally that of the client's OS)
 * Resolve the current locale via the given request,
 * should return a default locale as a fallback in any case.
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
public class CustomLocaleResolver extends AcceptHeaderLocaleResolver implements WebMvcConfigurer {
    List<Locale> LOCALES = Arrays.asList(
            new Locale(EN_LOCALE),
            new Locale(FR_LOCALE));

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String headerLang = request.getHeader(ACCEPT_LANGUAGE);
        return headerLang == null || headerLang.isEmpty()
                ? Locale.getDefault()
                : Locale.lookup(Locale.LanguageRange.parse(headerLang), LOCALES);
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(BASE_NAME);
        messageSource.setDefaultEncoding(DEFAULT_ENCODING);
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }
}
