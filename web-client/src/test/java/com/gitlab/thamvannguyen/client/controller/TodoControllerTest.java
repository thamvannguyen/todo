package com.gitlab.thamvannguyen.client.controller;


import com.fasterxml.jackson.core.type.TypeReference;
import com.gitlab.thamvannguyen.client.exception.InvalidInputException;
import com.gitlab.thamvannguyen.client.util.TranslationUtil;
import com.gitlab.thamvannguyen.core.util.json.JsonUtil;
import com.gitlab.thamvannguyen.core.util.json.TestDataLoader;
import com.gitlab.thamvannguyen.work.dao.entity.Work;
import com.gitlab.thamvannguyen.work.dto.Task;
import com.gitlab.thamvannguyen.work.exception.DateTimeFormatException;
import com.gitlab.thamvannguyen.work.exception.PaginationSortingException;
import com.gitlab.thamvannguyen.work.exception.StatusNotFoundException;
import com.gitlab.thamvannguyen.work.exception.WorkNotFoundException;
import com.gitlab.thamvannguyen.work.manager.WorkService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 *
 * @author thamnguyen (nguyenvanthamtclc@gmail.com)
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TodoControllerTest {

    private static final String ID_PARAM = "id";
    private static final String SIZE_PARAM = "size";
    private static final String PAGE_PARAM = "page";
    private static final String ORDER_BY_PARAM = "orderBy";
    private static final String DIRECTION_PARAM = "direction";

    private String id = "0750d21d-f9cb-46cb-9a86-1bf0a72a61ec";
    private String size = "10";
    private String pageIndex = "0";
    private String workPageFile = "WorkPage.json";
    private String workFileName = "Work.json";
    private String taskFileName = "Task.json";

    private MockMvc mockMvc;

    private TestDataLoader testDataLoader = TestDataLoader.builder().path("/data/").build();

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Mock
    private WorkService workService;

    @InjectMocks
    private TodoController todoController;

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(todoController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();
    }

    @Test
    public void shouldThrowStatusNotFoundExceptionWhenCreateWorkByInValidStatus() throws Exception {
        Task model = prepareTask();
        String className = StatusNotFoundException.class.getName();

        Mockito.when(workService.createWork(Mockito.any(Task.class))).thenThrow(StatusNotFoundException.class);

        mockMvc.perform(post("/todo/create")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).createWork(model);
        verifyNoMoreInteractions(workService);
    }

    @Test
    public void shouldThrowDateTimeFormatExceptionWhenCreateWorkByWrongDateTimeFormat() throws Exception {
        Task model = prepareTask();
        String className = DateTimeFormatException.class.getName();

        Mockito.when(workService.createWork(Mockito.any(Task.class))).thenThrow(DateTimeFormatException.class);

        mockMvc.perform(post("/todo/create")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).createWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldReturnDetailOfWorkWhenCreateWorkSuccess() throws Exception {
        Task model = prepareTask();
        Work expectedResult = prepareWork();

        Mockito.when(workService.createWork(Mockito.any(Task.class))).thenReturn(expectedResult);

        mockMvc.perform(post("/todo/create")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(expectedResult.getId())))
                .andExpect(jsonPath("$.workName", is(expectedResult.getWorkName())))
                .andExpect(jsonPath("$.startingDate").exists())
                .andExpect(jsonPath("$.endingDate").exists())
                .andExpect(jsonPath("$.status", is(expectedResult.getStatus().name())));

        verify(workService, times(1)).createWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowInvalidInputExceptionWhenUpdateWorkWithEmptyId() throws Exception {
        Task model = prepareTask();
        model.setId(StringUtils.EMPTY);
        String className = InvalidInputException.class.getName();

        mockMvc.perform(patch("/todo/update")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldThrowWorkNotFoundExceptionWhenUpdateWorkWithIdNotFound() throws Exception {
        Task model = prepareTask();
        String className = WorkNotFoundException.class.getName();

        Mockito.when(workService.updateWork(Mockito.any(Task.class))).thenThrow(WorkNotFoundException.class);

        mockMvc.perform(patch("/todo/update")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).updateWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowStatusNotFoundExceptionWhenUpdateWorkByInvalidStatus() throws Exception {
        Task model = prepareTask();
        String className = StatusNotFoundException.class.getName();

        Mockito.when(workService.updateWork(Mockito.any(Task.class))).thenThrow(StatusNotFoundException.class);

        mockMvc.perform(patch("/todo/update")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).updateWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowDateTimeFormatExceptionWhenUpdateWorkByWrongDateTimeFormat() throws Exception {
        Task model = prepareTask();
        String className = DateTimeFormatException.class.getName();

        Mockito.when(workService.updateWork(Mockito.any(Task.class))).thenThrow(DateTimeFormatException.class);

        mockMvc.perform(patch("/todo/update")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).updateWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldReturnIdWhenUpdateWorkSuccess() throws Exception {
        Task model = prepareTask();

        Mockito.when(workService.updateWork(Mockito.any(Task.class))).thenReturn(model.getId());

        mockMvc.perform(patch("/todo/update")
                .content(JsonUtil.convertObjectToJson(model))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(model.getId())));

        verify(workService, times(1)).updateWork(model);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowInvalidInputExceptionWhenDeleteWorkWithEmptyId() throws Exception {
        String className = InvalidInputException.class.getName();

        mockMvc.perform(delete("/todo/delete")
                .param(ID_PARAM, StringUtils.EMPTY)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldThrowWorkNotFoundExceptionWhenDeleteWorkWithIdNotFound() throws Exception {
        String className = WorkNotFoundException.class.getName();

        Mockito.when(workService.deleteWork(Mockito.anyString())).thenThrow(WorkNotFoundException.class);

        mockMvc.perform(delete("/todo/delete")
                .param(ID_PARAM, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).deleteWork(Mockito.anyString());
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldReturnIdWhenDeleteWorkSuccess() throws Exception {
        Mockito.when(workService.deleteWork(Mockito.anyString())).thenReturn(id);

        mockMvc.perform(delete("/todo/delete")
                .param(ID_PARAM, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(id)));

        verify(workService, times(1)).deleteWork(id);
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowInvalidInputExceptionWhenGetWorkByEmptyId() throws Exception {
        String className = InvalidInputException.class.getName();

        mockMvc.perform(get("/todo/get")
                .param(ID_PARAM, StringUtils.EMPTY))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldThrowWorkNotFoundExceptionWhenGetWorkWithIdNotFound() throws Exception {
        String className = WorkNotFoundException.class.getName();

        Mockito.when(workService.getWork(Mockito.anyString())).thenThrow(WorkNotFoundException.class);

        mockMvc.perform(get("/todo/get")
                .param(ID_PARAM, id))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));

        verify(workService, times(1)).getWork(Mockito.anyString());
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldReturnDetailOfWorkWhenGetWorkSuccess() throws Exception {
        Work expectedResult = prepareWork();

        Mockito.when(workService.getWork(Mockito.anyString())).thenReturn(expectedResult);

        mockMvc.perform(get("/todo/get")
                .param(ID_PARAM,id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(expectedResult.getId())))
                .andExpect(jsonPath("$.workName", is(expectedResult.getWorkName())))
                .andExpect(jsonPath("$.startingDate").exists())
                .andExpect(jsonPath("$.endingDate").exists())
                .andExpect(jsonPath("$.status", is(expectedResult.getStatus().name())));

        verify(workService, times(1)).getWork(Mockito.anyString());
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowPaginationSortingExceptionWhenFetchAllWorkWithIdNotFound() throws Exception {
        /*
        *
        * by default, constructor instance of AbstractPageRequest
        * with page < 0  or size < 1 will throw an IllegalArgumentException
        *
        * */
        int pageLessThanZero = -1;
        int sizeGreaterThanOrEqualOne = 1;
        String className = PaginationSortingException.class.getName();
        MultiValueMap<String, String> params = createParams(pageLessThanZero, sizeGreaterThanOrEqualOne);

        Mockito.when(workService.fetchAllWork(Mockito.anyInt(), Mockito.anyInt())).thenThrow(PaginationSortingException.class);


        mockMvc.perform(get("/todo/fetch")
                .params(params))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldReturnListOfWorkWhenFetchAllWorkSuccess() throws Exception {
        Page<Work> expectedResult = prepareWorkPage();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(PAGE_PARAM, pageIndex);
        params.add(SIZE_PARAM, String.valueOf(expectedResult.getSize()));

        Mockito.when(workService.fetchAllWork(Mockito.anyInt(), Mockito.anyInt())).thenReturn(expectedResult);

        mockMvc.perform(get("/todo/fetch")
                .params(params))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedResult.getSize())));

        verify(workService, times(1)).fetchAllWork(Mockito.anyInt(), Mockito.anyInt());
        verifyNoMoreInteractions(workService);
    }


    @Test
    public void shouldThrowPaginationSortingExceptionWhenFetchAllWorkByOrderByInvalidDirection() throws Exception {
        String className = PaginationSortingException.class.getName();
        MultiValueMap<String, String> params = prepareRequestParam("Wrong direction", "workName");

        mockMvc.perform(get("/todo/fetch")
                .params(params))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldThrowPaginationSortingExceptionWhenFetchAllWorkByOrderByInvalidOrderBy() throws Exception {
        String className = PaginationSortingException.class.getName();
        MultiValueMap<String, String> params = prepareRequestParam("ASC", "Invalid order by");

        mockMvc.perform(get("/todo/fetch")
                .params(params))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error_code", is(TranslationUtil.getErrorCode(className))))
                .andExpect(jsonPath("$.error_message", is(TranslationUtil.getErrorMessage(className))));
    }


    @Test
    public void shouldReturnListOfWorkWhenFetchAllWorkByOrderSuccess() throws Exception {
        Page<Work> expectedResult = prepareWorkPage();
        MultiValueMap<String, String> params = prepareRequestParam("ASC", "workName");

        Mockito.when(workService.fetchAllWork(Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyString())).thenReturn(expectedResult);

        mockMvc.perform(get("/todo/fetch")
                .params(params))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedResult.getSize())));

        verify(workService, times(1)).fetchAllWork(Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyString());
        verifyNoMoreInteractions(workService);
    }


    private MultiValueMap<String, String> prepareRequestParam(String direction, String workName) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(PAGE_PARAM, pageIndex);
        params.add(SIZE_PARAM, size);
        params.add(DIRECTION_PARAM, direction);
        params.add(ORDER_BY_PARAM, workName);
        return params;
    }


    private MultiValueMap<String, String> createParams(int pageLessThanZero, int sizeGreaterThanOrEqualOne) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(PAGE_PARAM, String.valueOf(pageLessThanZero));
        params.add(SIZE_PARAM, String.valueOf(sizeGreaterThanOrEqualOne));
        return params;
    }


    private Page<Work> prepareWorkPage() {

        return testDataLoader.loadInstance(workPageFile,new TypeReference<Page<Work>>() {});
    }


    private Work prepareWork() {

        return testDataLoader.loadInstance(workFileName, Work.class);
    }


    private Task prepareTask() {
        return testDataLoader.loadInstance(taskFileName, Task.class);
    }
}