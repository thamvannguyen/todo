# Sample ToDo List web application using Spring Boot and MySQL

A simple Todo list application with the below functions:

1. Create, edit, delete work.
2. Fetch a list of works with sorting and pagination
    - Fetch a list of works sort ascending by work name 
    - Fetch a list of works sort by given direction and orderBy param.

## Project structure
As a Java developer, I was responsible for building many full lifecycle web applications based Spring Framework, 
from initializing a new project to deploy into production. This project provides several modules such as configuration, core,web-client. Description for those modules following below: 

1. Configuration module: response for configuring entire project such as liquibase, information database connect
2. Core module: common utilities that can be customized for the specific project
3. Work module: depending on business will separator into a module that group entities which same related domain
4. Web-client module: module received a request from the client.

## Required
- Mysql version 5.7 
- Jdk 8 or more

To build and run the sample from a fresh clone of this repository and following bellow step:

## Configure MySQL
1. Create a database in your MySQL instance with name `todo_dev`.
2. In case create another name,
please update files environment-*.properties file in the `configuration/profiles/environment-*.properties` folder with the URL,
username and password for your MySQL instance. The table schema for the Todo will be generated for you in the database.

## Build and run the sample
1. `mvn clean install`
2. `cd web-client`
3. `java -jar target/ROOT.war`

#Run API

## Create work
url: localhost:8080/todo/create
body: E.g
{
    "workName": "Learn SpringBoot",
    "startingDate": "2019-05-07 07:54:00",
    "endingDate":  "2019-05-08 21:20:00",
    "status": "PLANNING"
}

## Update work
url: localhost:8080/todo/update
body: e.g
{
    "id":"0494c51c-8118-4587-b386-59b9c4143381",
    "workName": "Learn Ruby",
    "startingDate": "2019-05-08 08:00:00",
    "endingDate":  "2019-05-09 15:20:00",
    "status": "PLANNING"
}
## Delete work
localhost:8080/todo/delete?id=0494c51c-8118-4587-b386-59b9c4143381

## Fetch all work
- localhost:8080/todo/fetch?page=0&size=10
- localhost:8080/todov/fetch?orderBy=status&direction=DESC&page=0&size=10
